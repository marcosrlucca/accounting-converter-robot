FROM registry.sicredi.in/sicredi-openjdk11:latest
EXPOSE 8080
#ENV JAVA_OPTS -Duser.timezone=America/Sao_Paulo
ADD /build/libs/accounting-converter-robot*.jar /opt/api.jar
ENTRYPOINT exec java $JAVA_OPTS -jar /opt/api.jar