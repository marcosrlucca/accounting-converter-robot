package io.sicredi.accounting.converter.robot.api.controller;

import io.sicredi.accounting.converter.robot.Application;
import io.sicredi.accounting.converter.robot.api.dto.Raw;
import io.sicredi.accounting.converter.robot.business.service.ConverterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class)
class ConverterControllerTest {

    private static final String URI_MOVEMENT = "/convert/movement";
    private static final String URI_BALANCE = "/convert/balance";

    private static final String MOVEMENT_JSON_REVERSAL_ERROR = "{\"baseDate\":\"11/06/28\",\"operationDate\":\"28/05/13\",\"system\":\"vCSmRmAYPfslyxrInIBkoFyDdG\",\"company\":\"ZUN\",\"event\":\"uURzvDbNZaEf\",\"eventProduct\":\"MeYiRsgJeJQesKSeLXAfJnTfnPnh\",\"originId\":\"dVIDNFefsOZcFyvyTAwjpANkxJJfH\",\"transactionId\":\"HMuzntJTMHeaBQymRjlNDkTbuilF\",\"document\":\"GBbZeiUjR\",\"rule1\":\"dksLWjupUKshaXUpmxSzgmzSZngMbm\",\"rule2\":\"GHKQfzs\",\"rule3\":\"cSNgTbaoaborvEuHeyAUbRpptiIr\",\"rule4\":\"AJDUEVeqHQdPRdJxIQIyCJBSDWuAQ\",\"rule5\":\"LOgGMnPhKoaIaVVmnEesTEexaV\",\"rule6\":\"zvPaqXfTRRUPVnzbKsqmWZu\",\"rule7\":\"kAHEGTwPnkEjRxkFsqtAQefCa\",\"rule8\":\"vKPkDrkudkcCsmmtjIqLZVhwQac\",\"rule9\":\"tJJArqqWUsYyCgVaB\",\"rule10\":\"eMhxawNdna\",\"originalEventBaseDateForReversal\":\"03/01/19\",\"debitUnit\":\"zeTEqLmjWGQjiNu\",\"creditUnit\":\"wdHyFWcDbXbgXNYtcDBbnUPdzKr\",\"value1\":\"TpRjDgtzSWuoSDiZGAReYVUuwTeJVU\",\"value2\":\"JatVuoEGZXyHnGj\",\"value3\":\"TlPpND\",\"value4\":\"nWGSizNgCo\",\"value5\":\"LSUxIiEXMDDZTzoaGFbMe\",\"value6\":\"rIusqDnc\",\"value7\":\"lSUgduIVQYyg\",\"value8\":\"ZLPujqbmIbnAXrRvHDNICUX\",\"value9\":\"ryXFkOvSKerZQOtWBYxLOgEUvhwkaH\",\"value10\":\"FmvxelthuUgS\",\"reversal\":false}";
    private static final String MOVEMENT_JSON_OK = "{\"baseDate\":\"11/06/28\",\"operationDate\":\"28/05/13\",\"system\":\"vCSmRmAYPfslyxrInIBkoFyDdG\",\"company\":\"ZUN\",\"event\":\"uURzvDbNZaEf\",\"eventProduct\":\"MeYiRsgJeJQesKSeLXAfJnTfnPnh\",\"originId\":\"dVIDNFefsOZcFyvyTAwjpANkxJJfH\",\"transactionId\":\"HMuzntJTMHeaBQymRjlNDkTbuilF\",\"document\":\"GBbZeiUjR\",\"rule1\":\"dksLWjupUKshaXUpmxSzgmzSZngMbm\",\"rule2\":\"GHKQfzs\",\"rule3\":\"cSNgTbaoaborvEuHeyAUbRpptiIr\",\"rule4\":\"AJDUEVeqHQdPRdJxIQIyCJBSDWuAQ\",\"rule5\":\"LOgGMnPhKoaIaVVmnEesTEexaV\",\"rule6\":\"zvPaqXfTRRUPVnzbKsqmWZu\",\"rule7\":\"kAHEGTwPnkEjRxkFsqtAQefCa\",\"rule8\":\"vKPkDrkudkcCsmmtjIqLZVhwQac\",\"rule9\":\"tJJArqqWUsYyCgVaB\",\"rule10\":\"eMhxawNdna\",\"originalEventBaseDateForReversal\":\"03/01/19\",\"debitUnit\":\"zeTEqLmjWGQjiNu\",\"creditUnit\":\"wdHyFWcDbXbgXNYtcDBbnUPdzKr\",\"value1\":\"TpRjDgtzSWuoSDiZGAReYVUuwTeJVU\",\"value2\":\"JatVuoEGZXyHnGj\",\"value3\":\"TlPpND\",\"value4\":\"nWGSizNgCo\",\"value5\":\"LSUxIiEXMDDZTzoaGFbMe\",\"value6\":\"rIusqDnc\",\"value7\":\"lSUgduIVQYyg\",\"value8\":\"ZLPujqbmIbnAXrRvHDNICUX\",\"value9\":\"ryXFkOvSKerZQOtWBYxLOgEUvhwkaH\",\"value10\":\"FmvxelthuUgS\",\"reversal\":\"N\"}";
    private static final String BALANCE_JSON_OK = "{\"baseDate\":\"11/06/28\",\"operationDate\":\"28/05/13\",\"system\":\"vCSmRmAYPfslyxrInIBkoFyDdG\",\"company\":\"ZUN\",\"event\":\"uURzvDbNZaEf\",\"eventProduct\":\"MeYiRsgJeJQesKSeLXAfJnTfnPnh\",\"originId\":\"dVIDNFefsOZcFyvyTAwjpANkxJJfH\",\"transactionId\":\"HMuzntJTMHeaBQymRjlNDkTbuilF\",\"document\":\"GBbZeiUjR\",\"rule1\":\"dksLWjupUKshaXUpmxSzgmzSZngMbm\",\"rule2\":\"GHKQfzs\",\"rule3\":\"cSNgTbaoaborvEuHeyAUbRpptiIr\",\"rule4\":\"AJDUEVeqHQdPRdJxIQIyCJBSDWuAQ\",\"rule5\":\"LOgGMnPhKoaIaVVmnEesTEexaV\",\"rule6\":\"zvPaqXfTRRUPVnzbKsqmWZu\",\"rule7\":\"kAHEGTwPnkEjRxkFsqtAQefCa\",\"rule8\":\"vKPkDrkudkcCsmmtjIqLZVhwQac\",\"rule9\":\"tJJArqqWUsYyCgVaB\",\"rule10\":\"eMhxawNdna\",\"unit\":\"wdHyFWcDbXbgXNYtcDBbnUPdzKr\",\"value\":\"TpRjDgtzSWuoSDiZGAReYVUuwTeJVU\"}";

    @Autowired
    private ApplicationContext context;

    @MockBean
    private ConverterService converterService;

    private WebTestClient webTestClient;

    @BeforeEach
    void setup() {
        this.webTestClient = WebTestClient.bindToApplicationContext(this.context).build();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Calling movement conversion with reversal with wrong value")
    void convertMovementTest_reversal_error() {
        webTestClient
                .post()
                .uri(URI_MOVEMENT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(MOVEMENT_JSON_REVERSAL_ERROR))
                .exchange()
                .expectStatus()
                .is4xxClientError();
    }

    @Test
    @DisplayName("Calling movement conversion sucessfully")
    void convertMovementTest_ok() {
        Mockito.when(converterService.convert(Mockito.any(Raw.class))).thenReturn(Mono.empty());

        webTestClient
                .post()
                .uri(URI_MOVEMENT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(MOVEMENT_JSON_OK))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Void.class);
    }

    @Test
    @DisplayName("Calling balance conversion sucessfully")
    void convertBalanceTest_ok() {
        Mockito.when(converterService.convert(Mockito.any(Raw.class))).thenReturn(Mono.empty());

        webTestClient
                .post()
                .uri(URI_BALANCE)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(BALANCE_JSON_OK))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Void.class);
    }

}