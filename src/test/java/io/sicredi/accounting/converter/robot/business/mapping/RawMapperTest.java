package io.sicredi.accounting.converter.robot.business.mapping;

import io.github.benas.randombeans.api.EnhancedRandom;
import io.sicredi.accounting.converter.robot.Application;
import io.sicredi.accounting.converter.robot.api.dto.RawBalance;
import io.sicredi.accounting.converter.robot.api.dto.RawMovement;
import io.sicredi.accounting.converter.robot.business.domain.Entry;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class)
class RawMapperTest {

    @Autowired
    private RawMapper mapper;

    private RawMovement movement;

    private RawBalance balance;

    @BeforeEach
    void setUp() {
        movement = EnhancedRandom.random(RawMovement.class);
        balance = EnhancedRandom.random(RawBalance.class);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Movement number format exception")
    void toEntryTest_movement_numberformatexception() {
        Assertions.assertThrows(NumberFormatException.class, () -> mapper.toEntry(movement));
    }

    @Test
    @DisplayName("Movement successfully mapped")
    void toEntryTest_movement_sucessfull() {
        movement.setValue1("1");
        movement.setValue2("1");
        movement.setValue3("1");
        movement.setValue4("1");
        movement.setValue5("1");
        movement.setValue6("1");
        movement.setValue7("1");
        movement.setValue8("1");
        movement.setValue9("1");
        movement.setValue10("1");

        final Optional<Entry> maybeResult = mapper.toEntry(movement);

        Assertions.assertTrue(maybeResult.isPresent());
    }

    @Test
    @DisplayName("Balance number format exception")
    void toEntryTest_balance_numberformatexception() {
        Assertions.assertThrows(NumberFormatException.class, () -> mapper.toEntry(balance));
    }

    @Test
    @DisplayName("Balance successfully mapped")
    void toEntryTest_balance_sucessfull() {
        balance.setValue("1");

        final Optional<Entry> maybeResult = mapper.toEntry(balance);

        Assertions.assertTrue(maybeResult.isPresent());
    }

}