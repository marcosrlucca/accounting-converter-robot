package io.sicredi.accounting.converter.robot.business.service;

import io.github.benas.randombeans.api.EnhancedRandom;
import io.sicredi.accounting.converter.robot.Application;
import io.sicredi.accounting.converter.robot.api.dto.Raw;
import io.sicredi.accounting.converter.robot.business.domain.Entry;
import io.sicredi.accounting.converter.robot.business.mapping.RawMapper;
import io.sicredi.accounting.converter.robot.messaging.producer.EngineProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;
import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class)
class ConverterServiceTest {

    @MockBean
    private RawMapper mapper;

    @MockBean
    private EngineProducer engineProducer;

    @Autowired
    private ConverterService service;

    private Raw raw;

    private Entry entry;

    @BeforeEach
    void setUp() {
        raw = EnhancedRandom.random(Raw.class);
        entry = EnhancedRandom.random(Entry.class);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Convert successfully")
    void convertTest_successfull() {
        Optional<Entry> maybeEntry = Optional.of(entry);

        Mockito.when(mapper.toEntry(raw)).thenReturn(maybeEntry);
        Mockito.when(engineProducer.send(entry)).thenReturn(true);

        StepVerifier.create(service.convert(raw))
                .verifyComplete();
    }

    @Test
    @DisplayName("Convert technical exception")
    void convertTest_technicalexception() {
        Optional<Entry> maybeEntry = Optional.of(entry);

        Mockito.when(mapper.toEntry(raw)).thenReturn(maybeEntry);
        Mockito.when(engineProducer.send(entry)).thenReturn(false);

        StepVerifier.create(service.convert(raw))
                .verifyErrorMessage("Failed");
    }

    @Test
    @DisplayName("Convert business exception")
    void convertTest_businessexception() {
        Optional<Entry> maybeEntry = Optional.empty();

        Mockito.when(mapper.toEntry(raw)).thenReturn(maybeEntry);
        Mockito.when(engineProducer.send(entry)).thenReturn(false);

        StepVerifier.create(service.convert(raw))
                .verifyErrorMessage("Could not parse the Raw Dto");
    }

}