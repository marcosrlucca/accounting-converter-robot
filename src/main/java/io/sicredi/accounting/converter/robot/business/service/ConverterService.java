package io.sicredi.accounting.converter.robot.business.service;

import io.sicredi.accounting.converter.robot.api.dto.Payload;
import io.sicredi.accounting.converter.robot.api.dto.Raw;
import io.sicredi.accounting.converter.robot.business.domain.Entry;
import io.sicredi.accounting.converter.robot.business.mapping.RawMapper;
import io.sicredi.accounting.converter.robot.messaging.producer.EngineProducer;
import io.sicredi.services.platform.error.BusinessException;
import io.sicredi.services.platform.error.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;

@Service
public class ConverterService {

    private static final Logger log = LoggerFactory.getLogger(ConverterService.class);

    private final RawMapper mapper;

    private final EngineProducer engineProducer;

    @Autowired
    public ConverterService(RawMapper mapper, EngineProducer engineProducer) {
        this.mapper = mapper;
        this.engineProducer = engineProducer;
    }

    public Mono<Void> convert(Payload<?> payload) {
        log.debug("starting conversion");

        return Mono.create(sink ->
                mapper.toEntry(payload)
                        .ifPresentOrElse(
                                e -> e.forEach(entry -> sendTo(sink, entry)),
                                () -> sink.error(new BusinessException("Could not parse the Payload Dto"))));
    }

    public Mono<Void> convert(Raw raw) {
        log.debug("starting conversion");

        return Mono.create(sink ->
                mapper.toEntry(raw)
                        .ifPresentOrElse(
                                e -> sendTo(sink, e),
                                () -> sink.error(new BusinessException("Could not parse the Raw Dto"))));
    }

    private void sendTo(MonoSink<Void> sink, Entry entry) {
        log.debug("gonna to send to kafka");
        boolean result = engineProducer.send(entry);

        if (result) {
            log.debug("sent successfully");
            sink.success();
        } else {
            log.error("sent failured");
            sink.error(new TechnicalException("Failed"));
        }
    }

}
