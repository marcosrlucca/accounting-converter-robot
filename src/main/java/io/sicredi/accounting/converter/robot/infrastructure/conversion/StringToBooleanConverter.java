package io.sicredi.accounting.converter.robot.infrastructure.conversion;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.Optional;

public class StringToBooleanConverter extends JsonDeserializer<Boolean> {

    private static final Logger log = LoggerFactory.getLogger(StringToBooleanConverter.class);

    @Override
    public Boolean deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        log.debug("deserializing string to boolean started");

        String source = p.getText();

        return
                Optional.of(source)
                        .map(s -> {
                            if (s.equalsIgnoreCase("S")) {
                                log.debug("string S sent");
                                return Boolean.TRUE;
                            } else if (s.equalsIgnoreCase("N")) {
                                log.debug("string N sent");
                                return Boolean.FALSE;
                            } else {
                                log.error("The value {} is not supported!", source);
                                throw new UnsupportedOperationException("The value " + source + " is not supported!");
                            }
                        })
                        .orElseThrow(() -> new UnsupportedOperationException("The value " + source + " is not supported!"));
    }

}
