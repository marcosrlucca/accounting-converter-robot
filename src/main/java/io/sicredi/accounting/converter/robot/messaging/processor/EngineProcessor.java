package io.sicredi.accounting.converter.robot.messaging.processor;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import static io.sicredi.accounting.converter.robot.messaging.processor.Topic.TOPIC_OUTPUT;

@Component
public interface EngineProcessor {

    @Output(TOPIC_OUTPUT)
    MessageChannel output();

}

final class Topic {

    final static String TOPIC_OUTPUT = "processing-output";

}