package io.sicredi.accounting.converter.robot.business.mapping;

import io.sicredi.accounting.converter.robot.api.dto.Payload;
import io.sicredi.accounting.converter.robot.api.dto.Raw;
import io.sicredi.accounting.converter.robot.api.dto.RawBalance;
import io.sicredi.accounting.converter.robot.api.dto.RawMovement;
import io.sicredi.accounting.converter.robot.business.domain.Balance;
import io.sicredi.accounting.converter.robot.business.domain.Entry;
import io.sicredi.accounting.converter.robot.business.domain.Movement;
import io.sicredi.services.platform.error.BusinessException;
import org.apache.commons.lang3.StringUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RawMapper {

    private static final Logger log = LoggerFactory.getLogger(RawMapper.class);

    public Optional<List<Entry>> toEntry(Payload<?> payload) {
        log.debug("starting conversion");

        return Optional.of(
                payload.getPayload()
                        .stream()
                        .map(a -> toEntry((Raw) a))
                        .map(a -> a.orElseThrow(() -> new BusinessException("Entry can not be empty")))
                        .collect(Collectors.toList())
        );
    }

    public Optional<Entry> toEntry(Raw raw) {
        log.debug("starting conversion");

        if (raw instanceof RawMovement) {
            log.debug("RawMovement type");
            return toEntry((RawMovement) raw).map(a -> a);
        } else if (raw instanceof RawBalance) {
            log.debug("RawBalance type");
            return toEntry((RawBalance) raw).map(a -> a);
        } else {
            log.error("Unsupported type");
            throw new UnsupportedOperationException("Unrecognized type of Raw");
        }
    }

    private Optional<Movement> toEntry(RawMovement raw) {
        return Optional.ofNullable(raw)
                .map(r -> {
                    final Movement movement = new Movement();

                    setCommonFields(r, movement);

                    Optional.ofNullable(r.getOriginalEventBaseDateForReversal()).ifPresent(o -> movement.setOriginalEventBaseDateForReversal(o.atStartOfDay(ZoneId.systemDefault())));
                    movement.setCreditUnit(r.getCreditUnit());
                    movement.setDebitUnit(r.getDebitUnit());
                    movement.setReversal(r.getReversal());
                    movement.setValues(Stream.of(
                            r.getValue1(),
                            r.getValue2(),
                            r.getValue3(),
                            r.getValue4(),
                            r.getValue5(),
                            r.getValue6(),
                            r.getValue7(),
                            r.getValue8(),
                            r.getValue9(),
                            r.getValue10())
                            .filter(StringUtils::isNotEmpty)
                            .filter(this::validateNumberFormat)
                            .map(BigDecimal::new)
                            .map(this::twoDecimalPlaces)
                            .collect(Collectors.toList()));

                    return movement;
                });
    }

    private boolean validateNumberFormat(String value) {
        try {
            new BigDecimal(value);
        } catch (NumberFormatException e) {
            throw e;
        }

        return true;
    }

    private Optional<Balance> toEntry(RawBalance raw) {
        return Optional.ofNullable(raw)
                .map(r -> {
                    final Balance balance = new Balance();

                    setCommonFields(r, balance);

                    Optional.ofNullable(StringUtils.isNotEmpty(r.getValue()) ? r.getValue() : null)
                            .filter(this::validateNumberFormat)
                            .ifPresent(v -> balance.setValue(twoDecimalPlaces(new BigDecimal(v))));

                    balance.setUnit(r.getUnit());

                    return balance;
                });
    }

    private void setCommonFields(Raw raw, Entry entry) {
        final Optional<Raw> maybeRaw = Optional.ofNullable(raw);
        final Optional<Entry> maybeEntry = Optional.ofNullable(entry);

        maybeRaw.ifPresentOrElse(r ->
                        maybeEntry.ifPresentOrElse(e -> {
                                    Optional.ofNullable(r.getBaseDate()).ifPresent(bd -> e.setBaseDate(bd.atStartOfDay(ZoneId.systemDefault())));
                                    e.setProductReferenceCode(r.getEventProduct());
                                    e.setAccountingReferenceCode(r.getEvent());
                                    e.setCompany(r.getCompany());
                                    e.setDocument(r.getDocument());
                                    e.setSystem(r.getSystem());
                                    e.setTransactionId(r.getTransactionId());
                                    e.setTransactionDetail(r.getOriginId());
                                    e.setRules(Stream.of(
                                            r.getRule1(),
                                            r.getRule2(),
                                            r.getRule3(),
                                            r.getRule4(),
                                            r.getRule5(),
                                            r.getRule6(),
                                            r.getRule7(),
                                            r.getRule8(),
                                            r.getRule9(),
                                            r.getRule10())
                                            .filter(StringUtils::isNotEmpty)
                                            .collect(Collectors.toList()));
                                },
                                () -> new BusinessException("Entry can not be empty")),
                () -> new BusinessException("Raw can not be empty"));
    }

    private BigDecimal twoDecimalPlaces(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_DOWN);
    }

}
