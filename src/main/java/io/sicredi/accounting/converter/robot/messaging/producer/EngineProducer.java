package io.sicredi.accounting.converter.robot.messaging.producer;

import io.sicredi.accounting.converter.robot.business.domain.Entry;
import io.sicredi.accounting.converter.robot.messaging.processor.EngineProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

@Service
@EnableBinding(EngineProcessor.class)
public class EngineProducer {

    private static final Logger log = LoggerFactory.getLogger(EngineProducer.class);

    private final EngineProcessor balanceProcessor;

    public EngineProducer(EngineProcessor balanceProcessor) {
        this.balanceProcessor = balanceProcessor;
    }

    public boolean send(Entry entry) {
        log.debug("sending to kafka");

        return balanceProcessor.output()
                .send(MessageBuilder
                        .withPayload(entry)
                        .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                        .build());
    }

}
