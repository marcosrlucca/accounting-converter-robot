package io.sicredi.accounting.converter.robot.business.domain;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

public class Movement extends Entry {

    private ZonedDateTime originalEventBaseDateForReversal;

    private Boolean reversal;

    private String creditUnit;

    private String debitUnit;

    private List<BigDecimal> values;

    public ZonedDateTime getOriginalEventBaseDateForReversal() {
        return originalEventBaseDateForReversal;
    }

    public void setOriginalEventBaseDateForReversal(ZonedDateTime originalEventBaseDateForReversal) {
        this.originalEventBaseDateForReversal = originalEventBaseDateForReversal;
    }

    public Boolean getReversal() {
        return reversal;
    }

    public void setReversal(Boolean reversal) {
        this.reversal = reversal;
    }

    public String getCreditUnit() {
        return creditUnit;
    }

    public void setCreditUnit(String creditUnit) {
        this.creditUnit = creditUnit;
    }

    public String getDebitUnit() {
        return debitUnit;
    }

    public void setDebitUnit(String debitUnit) {
        this.debitUnit = debitUnit;
    }

    public List<BigDecimal> getValues() {
        return values;
    }

    public void setValues(List<BigDecimal> values) {
        this.values = values;
    }

}
