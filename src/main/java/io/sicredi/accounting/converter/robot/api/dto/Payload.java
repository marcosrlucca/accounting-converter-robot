package io.sicredi.accounting.converter.robot.api.dto;

import java.util.List;

public class Payload<T> {

    private List<T> payload;

    public List<T> getPayload() {
        return payload;
    }

    public void setPayload(List<T> payload) {
        this.payload = payload;
    }

}
