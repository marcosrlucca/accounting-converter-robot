package io.sicredi.accounting.converter.robot.infrastructure.configuration;

import io.sicredi.services.platform.error.BusinessException;
import io.sicredi.services.platform.error.TechnicalException;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ServerWebInputException;
import io.sicredi.accounting.converter.robot.api.dto.Error;
import javax.naming.ServiceUnavailableException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

@ControllerAdvice
public class CustomExceptionHandler {

    private final MessageSource messageSource;

    public CustomExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<Error> businessExceptionHandler(NumberFormatException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<Error> businessExceptionHandler(DateTimeParseException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Error> businessExceptionHandler(BusinessException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(TechnicalException.class)
    public ResponseEntity<Error> businessExceptionHandler(TechnicalException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<Error> serviceUnavailableExceptionHandler(ServiceUnavailableException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(WebClientResponseException.class)
    public ResponseEntity<Error> webClientResponseExceptionHandler(WebClientResponseException exception, ServerHttpRequest request) {
        HttpStatus status = exception.getStatusCode();

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(ServerWebInputException.class)
    public ResponseEntity<Error> throwableHandler(ServerWebInputException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, NestedExceptionUtils.getMostSpecificCause(exception).getMessage(), request.getURI().getRawPath()));
    }

    private Error generateErrorDTO(HttpStatus status, String message, String path) {
        Error dto = new Error();

        dto.setReason(status.getReasonPhrase());
        dto.setStatus(status.value());
        dto.setMessage(message);
        dto.setTimestamp(ZonedDateTime.now().toString());
        dto.setPath(path);

        return dto;
    }

    private String getFieldErrorMessage(FieldError fieldError) {
        try {
            return this.messageSource.getMessage(fieldError.getDefaultMessage(), fieldError.getArguments(), LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            return fieldError.getDefaultMessage();
        }
    }

}
