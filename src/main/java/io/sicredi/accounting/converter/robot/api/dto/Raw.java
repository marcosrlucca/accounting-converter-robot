package io.sicredi.accounting.converter.robot.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;

public class Raw {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yy")
    private LocalDate baseDate; //DATA_BASE

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yy")
    private LocalDate operationDate; //DATA_OPERACAO

    private String system; //SISTEMA

    private String company; //EMPRESA

    private String event; //EVENTO

    private String eventProduct; //EVENTO

    private String originId; //IDENTIFICACAO_ORIGEM

    private String transactionId; //ID_TRANSACAO

    private String document; //DOCUMENTO_PESSOA

    private String rule1; //REGRA_01

    private String rule2; //REGRA_02

    private String rule3; //REGRA_03

    private String rule4; //REGRA_04

    private String rule5; //REGRA_05

    private String rule6; //REGRA_06

    private String rule7; //REGRA_07

    private String rule8; //REGRA_08

    private String rule9; //REGRA_09

    private String rule10; //REGRA_10

    public LocalDate getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(LocalDate baseDate) {
        this.baseDate = baseDate;
    }

    public LocalDate getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(LocalDate operationDate) {
        this.operationDate = operationDate;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEventProduct() {
        return eventProduct;
    }

    public void setEventProduct(String eventProduct) {
        this.eventProduct = eventProduct;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getRule1() {
        return rule1;
    }

    public void setRule1(String rule1) {
        this.rule1 = rule1;
    }

    public String getRule2() {
        return rule2;
    }

    public void setRule2(String rule2) {
        this.rule2 = rule2;
    }

    public String getRule3() {
        return rule3;
    }

    public void setRule3(String rule3) {
        this.rule3 = rule3;
    }

    public String getRule4() {
        return rule4;
    }

    public void setRule4(String rule4) {
        this.rule4 = rule4;
    }

    public String getRule5() {
        return rule5;
    }

    public void setRule5(String rule5) {
        this.rule5 = rule5;
    }

    public String getRule6() {
        return rule6;
    }

    public void setRule6(String rule6) {
        this.rule6 = rule6;
    }

    public String getRule7() {
        return rule7;
    }

    public void setRule7(String rule7) {
        this.rule7 = rule7;
    }

    public String getRule8() {
        return rule8;
    }

    public void setRule8(String rule8) {
        this.rule8 = rule8;
    }

    public String getRule9() {
        return rule9;
    }

    public void setRule9(String rule9) {
        this.rule9 = rule9;
    }

    public String getRule10() {
        return rule10;
    }

    public void setRule10(String rule10) {
        this.rule10 = rule10;
    }

}
