package io.sicredi.accounting.converter.robot.api.controller;

import io.sicredi.accounting.converter.robot.api.dto.*;
import io.sicredi.accounting.converter.robot.business.service.ConverterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "convert", consumes = "application/json", produces = "application/json")
public class ConverterController {

    private static final Logger log = LoggerFactory.getLogger(ConverterController.class);

    private final ConverterService service;

    @Autowired
    public ConverterController(ConverterService service) {
        this.service = service;
    }

    @PostMapping("/movements")
    public Mono<ResponseEntity<Object>> convertMovements(@RequestBody Payload<RawMovement> payload) {
        log.debug("conversion for Movements called");

        return convert(payload);
    }

    @PostMapping("/balances")
    public Mono<ResponseEntity<Object>> convertBalances(@RequestBody Payload<RawBalance> payload) {
        log.debug("conversion for Balances called");

        return convert(payload);
    }

    @PostMapping("/movement")
    public Mono<ResponseEntity<Object>> convertMovement(@RequestBody RawMovement movement) {
        log.debug("conversion for Movement called");

        return convert((Raw) movement);
    }

    @PostMapping("/balance")
    public Mono<ResponseEntity<Object>> convertBalance(@RequestBody RawBalance balance) {
        log.debug("conversion for Balance called");

        return convert((Raw) balance);
    }

    private Mono<ResponseEntity<Object>> convert(Raw raw) {
        return service.convert(raw)
                .map(result -> ResponseEntity.ok().build())
                .onErrorMap(result -> result);
    }

    private Mono<ResponseEntity<Object>> convert(Payload<?> payload) {
        return service.convert(payload)
                .map(result -> ResponseEntity.ok().build())
                .onErrorMap(result -> result);
    }

}