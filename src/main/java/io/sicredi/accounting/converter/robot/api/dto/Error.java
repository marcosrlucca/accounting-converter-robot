package io.sicredi.accounting.converter.robot.api.dto;

public class Error {

    private int status;

    private String timestamp;
    private String path;
    private String reason;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Error [timestamp=" + timestamp + ", path=" + path + ", status=" + status + ", reason=" + reason + ", message=" + message + "]";
    }

}