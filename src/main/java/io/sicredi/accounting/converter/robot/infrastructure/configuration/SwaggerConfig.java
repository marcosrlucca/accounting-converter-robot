package io.sicredi.accounting.converter.robot.infrastructure.configuration;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;
import java.util.Collection;

@Configuration
@EnableSwagger2WebFlux
public class SwaggerConfig {

    @Bean
    public Docket apiV1(TypeResolver typeResolver, @Value("${spring.application.name}") String applicationName,
                        @Value("${SERVER_CONTEXT_PATH:/}") final String contextPath,
                        @Value("${MAJOR_VERSION:v0}") final String version) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(version)
                .apiInfo(getApiInfo(version, applicationName))
                .select()
                .apis(RequestHandlerSelectors.basePackage("io.sicredi.accounting.converter.robot"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping(contextPath)
                .alternateTypeRules(
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(Flux.class, WildcardType.class),
                                typeResolver.resolve(Collection.class, WildcardType.class)),
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(Flux.class,
                                        typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(Collection.class, WildcardType.class)),
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(Mono.class,
                                        typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(WildcardType.class)),
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(Mono.class, WildcardType.class),
                                typeResolver.resolve(WildcardType.class)));
    }

    private ApiInfo getApiInfo(String version, String applicationName) {
        return new ApiInfoBuilder()
                .title(applicationName)
                .description(
                        "<b>Accounting Squad</b><br/>Converter Robot API")
                .version(version)
                .build();
    }

}
