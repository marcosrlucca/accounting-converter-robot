package io.sicredi.accounting.converter.robot.business.domain;

import java.time.ZonedDateTime;
import java.util.List;

public class Entry {

    private ZonedDateTime baseDate;

    private String document;

    private String company;

    private String accountingReferenceCode;

    private String productReferenceCode;

    private String transactionId;

    private String transactionDetail;

    private String system;

    private List<String> rules;

    public ZonedDateTime getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(ZonedDateTime baseDate) {
        this.baseDate = baseDate;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAccountingReferenceCode() {
        return accountingReferenceCode;
    }

    public void setAccountingReferenceCode(String accountingReferenceCode) {
        this.accountingReferenceCode = accountingReferenceCode;
    }

    public String getProductReferenceCode() {
        return productReferenceCode;
    }

    public void setProductReferenceCode(String productReferenceCode) {
        this.productReferenceCode = productReferenceCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(String transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public List<String> getRules() {
        return rules;
    }

    public void setRules(List<String> rules) {
        this.rules = rules;
    }

}
