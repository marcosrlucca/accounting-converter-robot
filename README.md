# accounting-converter-robot

Microservice Spring Boot que recebe dados via POST e os envia para o tópico de entrada (accounting-engine-events-v2) da Engine Contábil.

Esta API pode receber tanto evento do tipo de Movimento, quanto do tipo de Saldo.


**Arquitetura**

![Arquitetura](src/main/resources/images/accounting-converter-robot.png)



**Tecnologias**

* [JDK 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
* [Gradle (build tool)](https://gradle.org/)
* [Apache Kafka](https://kafka.apache.org/)


**Build**

Você precisa ter as configurações do Gradle para o Nexus do Digital.

Mais informações sobre como configurar podem ser encontradas na Wiki

Agora basta utilizar o gradle wrapper presente na raiz do projeto:

$> ./gradlew build

Para rodar o projeto:

$> ./gradlew bootRun


Swagger

* [Swagger](http://accounting-converter-robot.dev-sicredi.in/api/v1/swagger-ui.html)