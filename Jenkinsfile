/**
 * When needed, build variables can be used
 * to determine different paths. These are listed bellow:
 *
 * Commons variables available for every job
 *
 * env.SCM_PROJECT_NAME  = gitlab's project name
 *
 * env.SCM_URL           = gitlab's project url
 *
 * env.CHAT_ROOM         = chat room to notify build status
 *
 * env.BUILD_FLAVOR      = flavor environment where this build is
 * running (akka node11x, node10x, node8x, java8x, java11x, python2x, etc)
 *
 * env.JOB_TEMPLATE_TYPE = selected type of this job when generated
 * by our seeds (akka K8S, ECS, FLINK_1_6)
 **/

/**
 * Invoked by build-deploy-latest jobs.
 */
def buildDeployLatest() {

    // call gradlew function that will load gradle daemon from cache
    // and execute project clean and build
    stage('clean & build') {
        gradlew("clean build test")
    }

    // call sonarqube
    stage('sonar reports') {
        gradlew('sonarqube -x test')
    }

    // build docker image
    stage('build image') {
        buildImage()
    }

    // deploy application properties to consul
    stage('deploy application properties to consul') {
        deployConsulProperties()
    }

    // deploy docker image
    stage('deploy development snapshot') {
        deploy()
    }
}

/**
 * Invoked by release jobs.
 */
def release() {
    // call gradlew function that will load gradle daemon from cache
    stage('clean & build') {
        gradlew("clean build test")
    }
}

/**
 * Invoked by quality-gate jobs.
 */
def qualityGate(args = [:]) {

    // call gradlew function that will load gradle
    // daemon from cache and execute project tests
    stage('test') {
        gradlew("test")
    }

    // call sonarqube forwarding args to publish
    // project metrics to scm
    stage('sonar reports') {
        gradlew("sonarqube -x test ${args.SONAR_ARGS}")
    }
}

// make sure we return an instance of this scripts so jenkins pipeline
// can call our functions t
return this